<?php

class ModelProductOligo extends Model {
	private $imodule = 'oligo';
	private $imodule_route;
	private $imodule_model;
	private $imodule_extension_route;
	private $imodule_extension_type;
	private $extension_version;

	public function __construct($registry) {
		parent::__construct($registry);
		$this->load->config($this->imodule);
		$this->imodule_route = $this->config->get($this->imodule . '_route');
		$this->imodule_extension_route = $this->config->get($this->imodule . '_extension_route');
		$this->imodule_extension_type = $this->config->get($this->imodule . '_extension_type');
		$this->extension_version = $this->config->get($this->imodule . '_extension_version');
		$this->imodule_model = $this->{$this->config->get($this->imodule . '_model_property')};
	}
}