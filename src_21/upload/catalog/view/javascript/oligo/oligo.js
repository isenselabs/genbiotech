/* Oligo Catalog JS */

let oligoCount = 0;
let oligoSubtotals = [];
let oligoSuccessCounter = 0;
let successMessage;
let forRemoval = [];

function pushTemplateInstance(index) {
	let oligoContainer = $('#oligo-container');
	let template = $('#oligo_template').html();
	template = template.split('%INDEX%').join(index.toString());
	if (oligoCount != 0) {
		template = template.split('%ONEBASEDINDEX%').join((index+1).toString());
	} else {
		template = template.split('%ONEBASEDINDEX%').join('');
	}
	$('.add-oligo-btn').hide();
	oligoContainer.append(template);
	$('.add-oligo-btn').last().show();
}

function removeOligoInstance(index) {
	$('#oligo-container .product[data-index="'+index+'"]').remove();
	oligoCount--;
	oligoSubtotals = oligoSubtotals.splice(index,1);
	// reindex the oligos after the removed one
	$('#oligo-container .product').each(function(i) {
		if (i >= index) {
			$(this).attr('data-index',i);
			$(this).find('[name^="oligo"]').each(function(j) {
				let name = $(this).attr('name').split('oligo[' + (i+1) + ']').join('oligo['+i+']');
				$(this).attr('name', name);
			});
			$(this).find('[id^="oligo-"]').each(function(j) {
				let id = $(this).attr('id').split('oligo-' + (i+1) + '-').join('oligo-' + i + '-');
				$(this).attr('id', id);
			});
			$(this).find('[for^="oligo-"]').each(function(j) {
				let forAttr = $(this).attr('for').split('oligo-' + (i+1) + '-').join('oligo-' + i + '-');
				$(this).attr('for', forAttr);
			});
			let templateNameLabel = $('#oligo_template label[for="oligo-%INDEX%-input-option0"]').html();
			if (i != 0) {
				$(this).find('label').first().html(templateNameLabel.split('%ONEBASEDINDEX%').join(i+1));
			} else {
				$(this).find('label').first().html(templateNameLabel.split('%ONEBASEDINDEX%').join(''));
			}
		}
	});
	$('.add-oligo-btn').hide();
	$('.add-oligo-btn').last().show();
	if (oligoCount == 1) {
		$('.delete-oligo-btn').hide();
	}
}

function stripOligoIndexFromName(name, index) {
	return name.replace('oligo[' + index + '][','').replace(']',''); // replaces first occurance only
}

function addSingleOligoToCart(index, total) {
	var inputs = $('#oligo-container .product [name^="oligo['+index+']"]');
	var data = {};
	inputs.each(function (i) {
		let name = $(this).attr('name');
		let val = $(this).val();
		data[stripOligoIndexFromName(name,index)] = val;
		//data.push(stripOligoIndexFromName(name,index) + '=' + encodeURIComponent(val));
	});
	var successful = false;
	$.ajax({
		async: true,
 		url: 'index.php?route=checkout/cart/add_oligo',
 		type: 'post',
 		data: data,
 		dataType: 'json',
 		beforeSend: function(jqXHR, settings) {
 			//$('#button-cart').button('loading');
 			//return true;
 		},
 		complete: function() {
 			//$('#button-cart').button('reset');
 			if (index != (total-1)) {
				addSingleOligoToCart(index+1, total);
			} else {
				removeSuccessfullyAddedOligos();
				showSuccessMessage();
			}
 		},
 		success: function(json) {
 			$('.product[data-index="'+index+'"] .alert, .product[data-index="'+index+'"] .text-danger').remove();
 			$('.product[data-index="'+index+'"] .form-group').removeClass('has-error');
 
 			if (json['error']) {
 				if (json['error']['option']) {
 					for (i in json['error']['option']) {
 						var element = $('#oligo-'+index+'-input-option' + i.replace('_', '-'));
 
 						if (element.parent().hasClass('input-group')) {
 							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
 						} else {
 							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
 						}
 					}
 				}
 
 				if (json['error']['recurring']) {
 					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
 				}
 
 				// Highlight any found errors
 				$('.text-danger').parent().addClass('has-error');
 			}
 			
 			if (json['success']) {
 				successful = true;
 				oligoSuccessCounter += 1;
 				showSuccess();
 				successMessage = json;
 				forRemoval.push(index);
//				if (!Journal.showNotification(json['success'], json['image'], true)) {
//					$('.breadcrumb').after('<div class="alert alert-success success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
//				}
//				$('#cart-total').html(json['total']);
//				if (Journal.scrollToTop) {
//					$('html, body').animate({ scrollTop: 0 }, 'slow');
//				}
//				$('#cart ul').load('index.php?route=common/cart/info ul li');
			}
 		},
         error: function(xhr, ajaxOptions, thrownError) {
             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
         }
 	});
 	return successful;
}

function updateCart(cartId) {
	index = 0;
	var inputs = $('.product [name^="oligo['+index+']"]');
	var data = {};
	data.cart_id = cartId;
	inputs.each(function (i) {
		let name = $(this).attr('name');
		let val = $(this).val();
		data[stripOligoIndexFromName(name,index)] = val;
		//data.push(stripOligoIndexFromName(name,index) + '=' + encodeURIComponent(val));
	});
	$.ajax({
 		url: 'index.php?route=checkout/cart/update_oligo',
 		type: 'post',
 		data: data,
 		dataType: 'json',
 		beforeSend: function(jqXHR, settings) {
 			$('#button-cart').button('loading');
 			return true;
 		},
 		complete: function() {
 			$('#button-cart').button('reset');
 		},
 		success: function(json) {
 			$('.product[data-index="'+index+'"] .alert, .product[data-index="'+index+'"] .text-danger').remove();
 			$('.product[data-index="'+index+'"] .form-group').removeClass('has-error');
 
 			if (json['error']) {
 				if (json['error']['option']) {
 					for (i in json['error']['option']) {
 						var element = $('#oligo-'+index+'-input-option' + i.replace('_', '-'));
 
 						if (element.parent().hasClass('input-group')) {
 							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
 						} else {
 							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
 						}
 					}
 				}
 
 				if (json['error']['recurring']) {
 					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
 				}
 
 				// Highlight any found errors
 				$('.text-danger').parent().addClass('has-error');
 			}
 
 			if (json['success']) {
 				removeOligoInstance(index);
 				oligoSuccessCounter += 1;
 				showSuccessUpdate();

				if (!Journal.showNotification(json['success'], json['image'], true)) {
					$('.breadcrumb').after('<div class="alert alert-success success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}

				$('#cart-total').html(json['total']);

				if (Journal.scrollToTop) {
					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}

				$('#cart ul').load('index.php?route=common/cart/info ul li');
			}
 		},
         error: function(xhr, ajaxOptions, thrownError) {
             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
         }
 	});
}

function showSuccess() {
	// show the success flash message
	if (oligoSuccessCounter == 1) {
		$('#singular').show();
		$('#plural').hide();
	} else {
		if ($('#oligo-success-container span').length != 0) {
			$('#oligo-success-container span').html(oligoSuccessCounter);
		} else {
			let old = $('#oligo-success-container').html();
			old = old.split('%N%').join('<span>' + oligoSuccessCounter + '</span>');
			$('#oligo-success-container').html(old);
		}
		$('#singular').hide();
		$('#plural').show();
	}
	$('#oligo-success-container').show();
}

function showSuccessUpdate() {
	$('#singular').hide();
	$('#plural').hide();
	$('#update').show();
	$('#oligo-success-container').show();
}

function onAddOligoClick(e) {
	e.preventDefault();
	pushTemplateInstance(oligoCount);
	oligoCount++;
	oligoSubtotals.push(0);
	$('.delete-oligo-btn').show();
}

function onRemoveOligoClick(e) {
	e.preventDefault();
	removeOligoInstance($(this).closest('.product').data('index'))
}

function onClearFormClick(e) {
	e.preventDefault();
	$(this).closest('.product').find('input, select, textarea').val('');
}

function onAddToCartClick(e) {
	e.preventDefault();
	addToCartLoading();
	setTimeout(function() {
		$('#oligo-success-container').hide();
		oligoSuccessCounter = 0;
		forRemoval = [];
		addSingleOligoToCart(0, oligoCount); // calls the next one up to oligoCount
		// remove oligos
		// show success message
	}, 500);
}

function removeSuccessfullyAddedOligos() {
	for (i=forRemoval.length-1;i>=0;i--) {
		removeOligoInstance(forRemoval[i]);
	}
}

function showSuccessMessage() {
	// show the journal success message
	if (oligoSuccessCounter > 0) {
		if (!Journal.showNotification(successMessage['success'], successMessage['image'], true)) {
			$('.breadcrumb').after('<div class="alert alert-success success">' + successMessage['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
		}
		$('#cart-total').html(successMessage['total']);
		if (Journal.scrollToTop) {
			$('html, body').animate({ scrollTop: 0 }, 'slow');
		}
		$('#cart ul').load('index.php?route=common/cart/info ul li');
	}
	setTimeout(addToCartReset, 500);
}

function onUpdateCartClick(e) {
	e.preventDefault();
	$('#oligo-success-container').hide();
	oligoSuccessCounter = 0;
	updateCart(preselectedCartId);
}

function updateTotal(index) {
	let mod5 = $('[name="oligo['+index+'][option][1]"]').val();
	let sequence = $('[name="oligo['+index+'][option][2]"]').val();
	let mod3 = $('[name="oligo['+index+'][option][3]"]').val();
	let scale = $('[name="oligo['+index+'][option][5]"]').val();
	let modInternal = $('[name="oligo['+index+'][option][4]"]').val();
	let purification = $('[name="oligo['+index+'][option][6]"]').val();
	if (scale == '' || purification == '' || sequence == '') return;
	let subtotal = 0;
	if (mod5 != '') subtotal += oligoPricing[1][mod5][scale];
	if (mod3 != '') subtotal += oligoPricing[3][mod3][scale];
	if (modInternal != '') subtotal += oligoPricing[4][modInternal][scale];
	if (purification != '') subtotal += oligoPricing[6][purification][scale];
	subtotal += sequence.length * oligoPricing[5][scale];
	
	oligoSubtotals[index] = subtotal;
	let newTotal = 0;
	for (i=0;i<oligoCount;++i) {
		let q = $('[name="oligo['+index+'][quantity]"]').val();
		q = parseInt(q, 10);
		if (q !== NaN) {
			newTotal += oligoSubtotals[i]*q;
		}
	}
	// send off the subtotals for formatting
	$.ajax({
 		url: 'index.php?route=product/product/format_oligo_price',
 		type: 'get',
 		data: {subtotal: subtotal, total: newTotal},
 		dataType: 'json',
 		success: function(json) {
 			$('.product[data-index="'+index+'"] .form-subtotal-value').html(json.subtotal);
			$('.subtotal-text span').html(json.total);
 		}
 	});
}

function replacePrice(str, newPrice) {
	let start = str.lastIndexOf('(');
	let end = str.lastIndexOf(')');
	return str.substring(0,start) + '(' + newPrice + ')';
}

function updateSelects(index) {
	let scale = $('[name="oligo['+index+'][option][5]"]').val();
	let idMap = [1,3,4,6];
	$('#oligo-container .product[data-index="'+index+'"] select.array-select').each(function(i) {
		$(this).find('option').not('[value=""]').each(function (j) {
			'TAMRA ($0.00)';
			$(this).html(replacePrice($(this).html(), oligoPricingPretty[idMap[i]][j][scale]))
		});
	});
}

function onSelectChange(e) {
	let index = $(this).closest('.product').data('index');
	updateTotal(index);
}

function onScaleChange(e) {
	let index = $(this).closest('.product').data('index');
	updateTotal(index);
	updateSelects(index, $(this).val());
}

function validateOligoSequence() {
	var allowed = ['A','C','G','T','R','Y','S','W','K','M','B','D','H','V','N','I'];
	var val = $(this).val().toUpperCase();
	var newVal = '';
	for (let i=0;i<val.length;i++) {
		for (let j=0;j<allowed.length;j++) {
			if (val.charAt(i) == allowed[j]) {
				newVal += allowed[j];
				break;
			}
		}
	}
	$(this).val(newVal);
}

function addToCartLoading() {
	// update the add to cart button to show the loading spinner
	button = $('#button-cart');
	console.log('Set to loading');
	button.html(button.data('loading-text') + ' <img src="image/customoligo/loading-spinner.gif"/>');
}

function addToCartReset() {
	// resets the add to cart button to its original text, hiding the loading spinner
	button = $('#button-cart');
	console.log('Resetting', button.html());
	button.html(button.data('regular-text'));
	console.log('Done', button.html());
}

function fillOligos(n) {
	for (i=0;i<n;i++) {
		$('.add-oligo-btn').last().click();
	}
	$('[name$="[option][0]"').not('[name*="%INDEX%"]').val('Oligo name');
	$('[name$="[option][2]"').not('[name*="%INDEX%"]').val('ACGTACGT');
	$('[name$="[option][5]"').not('[name*="%INDEX%"]').val(0);
	$('[name$="[option][6]"').not('[name*="%INDEX%"]').val(0);
}

$(document).ready(function() {
	pushTemplateInstance(0);
	$('.delete-oligo-btn').hide();
	oligoCount = 1;
	oligoSubtotals.push(0);
	$('#button-cart').on('click', onAddToCartClick);
	$('#update-cart').on('click', onUpdateCartClick);
	// bind to parent with child selector to avoid rebinding when adding new forms
	$('#oligo-container').on('click', '.add-oligo-btn', onAddOligoClick);
	$('#oligo-container').on('click', '.delete-oligo-btn', onRemoveOligoClick);
	$('#oligo-container').on('click', '.clear-oligo-btn', onClearFormClick);
	if (typeof oligoPricing !== 'undefined') {
		$('#oligo-container').on('change', 'select.array-select, input.array-select, input[name$="[quantity]"]', onSelectChange);
		$('#oligo-container').on('change', 'select.scale-select', onScaleChange);
		if (typeof preselectedCartId !== 'undefined') {
			updateTotal(0);
		}
	}
	$('#oligo-container').on('change', 'input[name$="[option][2]"]', validateOligoSequence);
});