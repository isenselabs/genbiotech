<?php
/** Based on OC v2.1.0.0_rc1:/catalog/view/theme/default/product/product.tpl **/
echo $header;
?>
<div id="container" class="container j-container oligo-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row">
  <?php //echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="product-page-content" itemscope itemtype="http://schema.org/Product">
      <?php /*if ($this->journal2->settings->get('product_page_title_position', 'top') === 'top'):*/ ?>
      <h1 class="heading-title" itemprop="name"><?php echo $heading_title; ?></h1>
      <?php /*endif;*/ ?>
      <?php echo $content_top; ?>
      <div id="oligo_template">
        <div class="product" data-index="%INDEX%">
          <div class="row oligo-option-row">
            <div class="form-group required oligo-option-cell">
              <label class="control-label" for="oligo-%INDEX%-input-option0"><?php echo $options[0]['name']; ?> %ONEBASEDINDEX%</label>
              <input type="text" name="oligo[%INDEX%][option][0]" value="<?php echo $options[0]['value']; ?>" placeholder="<?php echo $options[0]['name']; ?>" id="oligo-%INDEX%-input-option0" class="form-control" />
            </div>
          </div>
          <div class="row oligo-option-row">
            <div class="form-group oligo-option-cell oligo-modification">
              <label class="control-label" for="oligo-%INDEX%-input-option1"><?php echo $options[1]['name']; ?></label>
              <select name="oligo[%INDEX%][option][1]" id="oligo-%INDEX%-input-option1" class="form-control array-select">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($options[1]['product_option_value'] as $option_value) { ?>
                <option value="<?php echo $option_value['product_option_value_id']; ?>"<?php echo (($preselected && isset($preselected['options'][1]) && $option_value['product_option_value_id'] == $preselected['options'][1]['product_option_value_id'])?' selected':'') ?>><?php echo $option_value['name']; ?>
                <?php if ($option_value['price'][0] && $show_prices) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price'][0]; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group required oligo-option-cell oligo-sequence">
              <label class="control-label" for="oligo-%INDEX%-input-option2"><?php echo $options[2]['name']; ?></label>
              <input type="text" name="oligo[%INDEX%][option][2]" value="<?php echo $options[2]['value']; ?>" placeholder="<?php echo $options[2]['name']; ?>" id="oligo-%INDEX%-input-option2" class="form-control array-select" />
            </div>
            <div class="form-group oligo-option-cell oligo-modification">
              <label class="control-label" for="oligo-%INDEX%-input-option1"><?php echo $options[3]['name']; ?></label>
              <select name="oligo[%INDEX%][option][3]" id="oligo-%INDEX%-input-option3" class="form-control array-select">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($options[3]['product_option_value'] as $option_value) { ?>
                <option value="<?php echo $option_value['product_option_value_id']; ?>"<?php echo (($preselected && isset($preselected['options'][3]) && $option_value['product_option_value_id'] == $preselected['options'][3]['product_option_value_id'])?' selected':'') ?>><?php echo $option_value['name']; ?>
                <?php if ($option_value['price'][0] && $show_prices) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price'][0]; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="row oligo-option-row">
            <div class="form-group oligo-option-cell">
              <label class="control-label" for="oligo-%INDEX%-input-option4"><?php echo $options[4]['name']; ?></label>
              <select name="oligo[%INDEX%][option][4]" id="oligo-%INDEX%-input-option4" class="form-control array-select">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($options[4]['product_option_value'] as $option_value) { ?>
                <option value="<?php echo $option_value['product_option_value_id']; ?>"<?php echo (($preselected && isset($preselected['options'][4]) && $option_value['product_option_value_id'] == $preselected['options'][4]['product_option_value_id'])?' selected':'') ?>><?php echo $option_value['name']; ?>
                <?php if ($option_value['price'][0] && $show_prices) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price'][0]; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group required oligo-option-cell">
              <label class="control-label" for="oligo-%INDEX%-input-option5"><?php echo $options[5]['name']; ?></label>
              <select name="oligo[%INDEX%][option][5]" id="oligo-%INDEX%-input-option5" class="form-control scale-select">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($options[5]['product_option_value'] as $option_value) { ?>
                <option value="<?php echo $option_value['product_option_value_id']; ?>"<?php echo (($preselected && isset($preselected['options'][5]) && $option_value['product_option_value_id'] == $preselected['options'][5]['product_option_value_id'])?' selected':'') ?>><?php echo $option_value['name']; ?>
                <?php if ($option_value['price'] && $show_prices) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group required oligo-option-cell">
              <label class="control-label" for="oligo-%INDEX%-input-option6"><?php echo $options[6]['name']; ?></label>
              <select name="oligo[%INDEX%][option][6]" id="oligo-%INDEX%-input-option6" class="form-control array-select">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($options[6]['product_option_value'] as $option_value) { ?>
                <option value="<?php echo $option_value['product_option_value_id']; ?>"<?php echo (($preselected && isset($preselected['options'][6]) && $option_value['product_option_value_id'] == $preselected['options'][6]['product_option_value_id'])?' selected':'') ?>><?php echo $option_value['name']; ?>
                <?php if ($option_value['price'][0] && $show_prices) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price'][0]; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-footer">
            <div class="form-footer-header">
              <span class="form-footer-header-title"><?php echo $text_oligo_ordering; ?></span>
              <?php if ($preselected === false): ?>
              <button class="btn btn-primary clear-oligo-btn"><i class="fa fa-times" aria-hidden="true"></i> <?php echo $text_oligo_clear_form; ?></button>
              <button class="btn btn-primary delete-oligo-btn"><i class="fa fa-times" aria-hidden="true"></i> <?php echo $text_oligo_delete_form; ?></button>
              <?php endif; ?>
            </div>
            <div class="form-footer-body">
                <div class="form-subtotal-container">
                  <div class="form-subtotal-label"><?php echo $text_oligo_subtotal; ?></div>
                  <div class="form-subtotal-value"><?php echo $zero_total; ?></div>
                </div>
                <div class="form-oligo-quiantity">
                  <label class="control-label" for="oligo-%INDEX%-input-quantity"><?php echo $entry_qty; ?></label>
                  <input type="text" name="oligo[%INDEX%][quantity]" value="<?php echo $minimum; ?>" size="2" id="oligo-%INDEX%-input-quantity" class="form-control" />
                  <input type="hidden" name="oligo[%INDEX%][product_id]" value="<?php echo $product_id; ?>" />
                  <br />
                </div>
                <div class="form-add-oligo-container">
                  <?php if ($preselected === false): ?>
                  <button class="btn btn-primary add-oligo-btn"><i class="fa fa-plus" aria-hidden="true"></i> <?php echo $text_oligo_add; ?></button>
                  <?php endif; ?>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div id="oligo-success-container" style="display: none;">
        <div id="plural" style="display: none;">
          <?php echo $text_successfully_added; ?>
        </div>
        <div id="singular" style="display: none;">
          <?php echo $text_successfully_added_singular; ?>
        </div>
        <div id="update" style="display: none;">
          <?php echo $text_successfully_updated; ?>
        </div>
      </div>
      <div id="oligo-container"></div>
      <div class="oligo-subtotal">
        <p class="subtotal-text">Subtotal: <span><?php echo $zero_total; ?></span></p>
        <?php if ($preselected === false) { ?>
        <button type="button" id="button-cart" data-regular-text="<?php echo $button_cart; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button>
        <?php } else { ?>
        <button type="button" id="update-cart" data-regular-text="<?php echo $button_update_cart; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_update_cart; ?></button>
        <?php } ?>
      </div>
    </div>
      <?php if ($products) { ?>
      <h3><?php echo $text_related; ?></h3>
      <div class="row">
        <?php $i = 0; ?>
        <?php foreach ($products as $product) { ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-lg-6 col-md-6 col-sm-12 col-xs-12'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-lg-4 col-md-4 col-sm-6 col-xs-12'; ?>
        <?php } else { ?>
        <?php $class = 'col-lg-3 col-md-3 col-sm-6 col-xs-12'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>">
          <div class="product-thumb transition">
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div class="caption">
              <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
              <p><?php echo $product['description']; ?></p>
              <?php if ($product['rating']) { ?>
              <div class="rating">
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($product['rating'] < $i) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } ?>
                <?php } ?>
              </div>
              <?php } ?>
              <?php if ($product['price']) { ?>
              <p class="price">
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                <?php } ?>
                <?php if ($product['tax']) { ?>
                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                <?php } ?>
              </p>
              <?php } ?>
            </div>
            <div class="button-group">
              <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span> <i class="fa fa-shopping-cart"></i></button>
              <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
              <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
            </div>
          </div>
        </div>
        <?php if (($column_left && $column_right) && ($i % 2 == 0)) { ?>
        <div class="clearfix visible-md visible-sm"></div>
        <?php } elseif (($column_left || $column_right) && ($i % 3 == 0)) { ?>
        <div class="clearfix visible-md"></div>
        <?php } elseif ($i % 4 == 0) { ?>
        <div class="clearfix visible-md"></div>
        <?php } ?>
        <?php $i++; ?>
        <?php } ?>
      </div>
      <?php } ?>
      <?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php //echo $column_right; ?></div>
</div>
<script type="text/javascript">
<?php if ($show_prices) { ?>
oligoPricing = <?php echo json_encode($oligoPricing); ?>;
oligoPricingPretty = <?php echo json_encode($oligoPricingPretty); ?>;
<?php } ?>
<?php if ($preselected !== false) { ?>
preselectedCartId = <?php echo $preselected['cart_id']; ?>;
<?php } ?>
</script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});

$('.datetime').datetimepicker({
  pickDate: true,
  pickTime: true
});

$('.time').datetimepicker({
  pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
  var node = this;

  $('#form-upload').remove();

  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="oligo[%INDEX%][file]" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');

  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }

  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);

      $.ajax({
        url: 'index.php?route=tool/upload',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {
          $('.text-danger').remove();

          if (json['error']) {
            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
          }

          if (json['success']) {
            alert(json['success']);

            $(node).parent().find('input').attr('value', json['code']);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
  $.ajax({
    url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
    type: 'post',
    dataType: 'json',
    data: $("#form-review").serialize(),
    beforeSend: function() {
      $('#button-review').button('loading');
    },
    complete: function() {
      $('#button-review').button('reset');
    },
    success: function(json) {
      $('.alert-success, .alert-danger').remove();

      if (json['error']) {
        $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }

      if (json['success']) {
        $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

        $('input[name=\'name\']').val('');
        $('textarea[name=\'text\']').val('');
        $('input[name=\'rating\']:checked').prop('checked', false);
      }
    }
  });
});

$(document).ready(function() {
  $('.thumbnails').magnificPopup({
    type:'image',
    delegate: 'a',
    gallery: {
      enabled:true
    }
  });
});
//--></script>
<?php echo $footer; ?>
