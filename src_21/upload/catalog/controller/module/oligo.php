<?php
class ControllerModuleOligo extends Controller {

	public function download() {
		if (!isset($this->request->get['order']) || !isset($this->request->get['h'])) return;
		$orderId = $this->request->get['order'];
		$inputHash = $this->request->get['h'];
		$this->load->model('module/oligo');
		$order = $this->model_module_oligo->getOrder($orderId);
		if (empty($order)) return;
		$filename = DIR_SYSTEM . 'storage/customoligos/'.$order['firstname'].'_'.$order['lastname'].'_order' . $orderId . '.xls';
		if (!file_exists($filename)) return;
		$fileHash = md5_file($filename);
		if ($fileHash != $inputHash) return;
		header('Content-Disposition: attachment; filename="'.$order['firstname'].'_'.$order['lastname'].'_order' . $orderId.'.xls"');
		readfile($filename);
		exit;
	}

}