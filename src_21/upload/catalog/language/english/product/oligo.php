<?php

// Heading
$_['heading_title']										= 'Oligo';
$_['text_oligo_ordering']                               = 'You are ordering:';
$_['text_oligo_clear_form']                             = 'Clear Form';
$_['text_oligo_delete_form']                            = 'Remove Oligo';
$_['text_oligo_subtotal']                               = 'Item subtotal:';
$_['text_oligo_add']                                    = 'Add Oligo';
$_['button_update_cart']                                = 'Update cart';

$_['oligo_form_name_label']                             = 'Oligo Name';
$_['oligo_form_5_mod_label']                            = "5' Modification";
$_['oligo_form_sequence_label']                         = 'Oligo Sequence';
$_['oligo_form_3_mod_label']                            = "3' Modification";
$_['oligo_form_internal_mod_label']                     = 'Add Internal Modification';
$_['oligo_form_scale_label']                            = 'Oligo Scale';
$_['oligo_form_purification_label']                     = 'Purification';

$_['error_oligo_sequence']                              = 'Invalid oligo sequence!';

$_['text_successfully_added']                           = 'Successfully added %N% oligos to your cart!';
$_['text_successfully_added_singular']                  = 'Successfully added 1 oligo to your cart!';

$_['text_successfully_updated']                         = 'Successfully updated your custom oligo!';