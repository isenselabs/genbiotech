<?php

namespace vendor\oligo;

class PHPExcel {

	private $moduleRoot;

	public function __construct($registry=null) {
		$this->moduleRoot = substr(DIR_APPLICATION, 0, strrpos(DIR_APPLICATION, '/', -2));
		require_once($this->moduleRoot . '/vendors/phpexcel/PHPExcel.php');
		require_once($this->moduleRoot . '/vendors/phpexcel/PHPExcel/IOFactory.php');
	}

	public function loadTemplate() {
		$templateFilename = __DIR__ . '/template.xls';
		try {
			$fileType = \PHPExcel_IOFactory::identify($templateFilename);
			$objReader = \PHPExcel_IOFactory::createReader($fileType);
			$objPHPExcel = $objReader->load($templateFilename);
			return $objPHPExcel;
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($templateFilename,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
	}

	public function save($objPHPExcel, $filename) {
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->setPreCalculateFormulas(true);
		$objWriter->save($filename);
	}

	public function generate($order, $products, $model) {
		usort($products, function($a, $b) { return $a['order_product_id'] - $b['order_product_id']; });
		$options = [];
		foreach ($products as $key => $product) {
			if ($product['product_id'] == $model->config->get('oligo_product_id')) {
				$options['_'.$product['order_product_id']] = $model->getOrderOptions($order['order_id'], $product['order_product_id']);
			}
		}

		if (count($options) == 0) return null;

		$excel = $this->loadTemplate();

		$sheet = $excel->getActiveSheet();
		// Order ID
		$sheet->setCellValue('C2', $order['order_id']);
		// Date
		$sheet->setCellValue('C3', $order['date_modified']);

		// Shipping info
		$sheet->setCellValue('F15', $order['shipping_postcode']);
		$sheet->setCellValue('F16', $order['shipping_city']);
		$sheet->setCellValue('F17', $order['shipping_zone']);

		// Company / Inst .:
		$company = $order['payment_company'];
		if (!empty($order['payment_company'])) {
			$company = $order['payment_company'];
		} else if (!empty($order['shipping_company'])) {
			$company = $order['shipping_company'];
		} else {
			$company = '';
		}
		$sheet->setCellValue('C11', $company);
		// Researcher / Project:
		$sheet->setCellValue('C12', $order['customer']);
		// Billing information
		// ??
		// Phone:
		$sheet->setCellValue('C14', $order['telephone']);
		// Fax:
		$sheet->setCellValue('C15', $order['fax']);
		// Email:
		$sheet->setCellValue('C16', $order['email']);
		// Mobile Phone:
		$sheet->setCellValue('C17', $order['telephone']);
		//        Other Information:
		$sheet->setCellValue('C18', $order['comment']);

		// Oligos
		$font = $sheet->getStyle('H22')->getFont();
		$counter = 0;
		foreach ($options as $k => $optionSet) {
			$oligoOptions = [];
			foreach ($optionSet as $opt) {
				if ($opt['type'] == '.') continue;
				$oligoOptions[$opt['product_option_id']] = $opt['value'];
			}
			$row = 22+$counter;
			$sheet->setCellValue('A'.$row, $counter+1);
			if (isset($oligoOptions[0])) $sheet->setCellValue('B'.$row, $oligoOptions[0]);
			if (isset($oligoOptions[2])) {
				$sheet->setCellValue('C'.$row, $oligoOptions[2]);
				$sheet->setCellValue('D'.$row, '=LEN(C' . $row . ')');
			}
			//$centered = array('alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
			for ($col = 'A'; $col != 'J'; ++$col) {
				//$sheet->getStyle($col . $row)->setFont($font);
				/*$style = $sheet->getStyleByColumnAndRow($col . 23);
				$sheet->duplicateStyle($style, $col . $row);*/
				$cellFrom = $sheet->getCell($col.'22');
				$cellTo = $sheet->getCell($col.$row);
				$cellTo->setXfIndex($cellFrom->getXfIndex());
				//if ($col != 'A') $cellTo->getStyle()->applyFromArray($centered);
			}
			$height = $sheet->getRowDimension(22)->getRowHeight();
			$sheet->getRowDimension($row)->setRowHeight($height);

			if (isset($oligoOptions[5])) $sheet->setCellValue('E'.$row, $oligoOptions[5]);
			if (isset($oligoOptions[1])) $sheet->setCellValue('F'.$row, $oligoOptions[1]);
			if (isset($oligoOptions[3])) $sheet->setCellValue('G'.$row, $oligoOptions[3]);
			if (isset($oligoOptions[4])) $sheet->setCellValue('H'.$row, $oligoOptions[4]);
			if (isset($oligoOptions[6])) $sheet->setCellValue('I'.$row, $oligoOptions[6]);
			++$counter;
		}

		$style = array(
			'alignment' => array(
				'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
		);

		$sheet->getStyle('B22:I' . (22+$counter))->applyFromArray($style);

		// copy the info under the table
		$modSheet = $excel->getSheet(1);
		$copy = $modSheet->rangeToArray('H2:L12');
		$sheet->fromArray($copy, null, 'C' . ($counter+24));
		
		$temp = array(
			array('H', 'C'),
			array('I', 'D'),
			array('J', 'E'),
			array('K', 'F'),
			array('L', 'G')
		);
		
		$allBorders = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb'=>\PHPExcel_Style_Color::COLOR_BLACK)
				)
			)
		);

		$bolded = array(
			'font' => array(
				'bold' => true,
			)
		);

		$sheet->getStyle('G' . ($counter+25) . ':G' . ($counter+28))->applyFromArray($allBorders);
		$sheet->getStyle('G' . ($counter+30) . ':G' . ($counter+34))->applyFromArray($allBorders);

		$sheet->getStyle('C' . ($counter+24))->applyFromArray($bolded);
		$sheet->getStyle('G' . ($counter+25))->applyFromArray($bolded);
		$sheet->getStyle('G' . ($counter+30))->applyFromArray($bolded);

		$sheet->getStyle('C' . ($counter+24) . ':G' . ($counter+35))->getFont()->setSize(12);

		return $excel;
	}
}
