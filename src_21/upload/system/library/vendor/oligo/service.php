<?php

namespace vendor\oligo;

class Service {
	private $imodule = 'oligo';
	private $registry;

	public function __construct($registry) {
		$this->registry = $registry;
		$extractFromRegistry = array('load','config','customer','language');
		foreach ($extractFromRegistry as $key) $this->{$key} = $this->registry->get($key);

		$this->load->model('setting/setting');
		$this->load->language('product/oligo');
		$this->model_setting_setting = $this->registry->get('model_setting_setting');
	}

	public function loadSettings() {
		return $this->model_setting_setting->getSetting($this->imodule);
	}

	public function modSettings($changes) {
		$group_key = version_compare(VERSION, '2.0.0.0', '>') ? 'code' : 'group';
		foreach ($settings as $key => $value) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE `" . $group_key . "`='" . $this->imodule . "' AND `key`='" . $key . "'");
			$this->db->query("INSERT INTO `" . DB_PREFIX . "setting` SET `" . $group_key . "`='" . $this->imodule . "', `key`='" . $key . "', `value`='" . $this->db->escape($value) . "', serialized=0, store_id=0");
		}
	}

	public function renderProductOptions() {
		$settings = $this->loadSettings();
		$showPrices = (!$this->config->get('config_customer_price') || $this->customer->isLogged());
		$pricing = $this->config->get('oligo_pricing');
		return array(
			$this->renderOptionName($showPrices, $pricing[0]),
			$this->renderOption5Mod($showPrices, $pricing[1]),
			$this->renderOptionSequence($showPrices, $pricing[2]),
			$this->renderOption3Mod($showPrices, $pricing[3]),
			$this->renderOptionInternalMod($showPrices, $pricing[4]),
			$this->renderOptionScale($showPrices, $pricing[5]),
			$this->renderOptionPurification($showPrices, $pricing[6])
		);
	}

	public function renderProductOptionData($options) {
		$settings = $this->loadSettings();
		$pricing = $this->config->get('oligo_pricing');
		if (isset($options[1]) && $options[1] != '') { $renderOption5Mod = $this->renderOption5Mod(false, $pricing[1]); /* print_r($options[1]); print_r($renderOption5Mod); */ } else $renderOption5Mod = false;
		if (isset($options[3]) && $options[3] != '') { $renderOption3Mod = $this->renderOption3Mod(false, $pricing[3]); /* print_r($options[3]); print_r($renderOption3Mod); */ } else $renderOption3Mod = false;
		if (isset($options[4]) && $options[4] != '') { $renderOptionInternalMod = $this->renderOptionInternalMod(false, $pricing[4]); /* print_r($options[4]); print_r($renderOptionInternalMod); */ } else $renderOptionInternalMod = false;
		if (isset($options[5]) && $options[5] != '') { $renderOptionScale = $this->renderOptionScale(false, $pricing[5]); /* print_r($options[5]); print_r($renderOptionScale); */ } else $renderOptionScale = false;
		if (isset($options[6]) && $options[6] != '') { $renderOptionPurification = $this->renderOptionPurification(false, $pricing[6]); /* print_r($options[6]); print_r($renderOptionPurification); */ } else $renderOptionPurification = false;
		
		$option_data = [];
		$option_data['_'] = '.'; // force array keys to be serialized
		if (isset($options[0])) $option_data[0] = $this->renderOptionDataName($options[0], $options[0], $pricing[0]);
		if (isset($options[1])) $option_data[1] = $this->renderOptionData5Mod($options[1],($renderOption5Mod)?$renderOption5Mod['product_option_value'][$options[1]]['name']:'', $pricing[1]);
		if (isset($options[2])) $option_data[2] = $this->renderOptionDataSequence($options[2], $options[2], $pricing[2]);
		if (isset($options[3])) $option_data[3] = $this->renderOptionData3Mod($options[3],($renderOption3Mod)?$renderOption3Mod['product_option_value'][$options[3]]['name']:'', $pricing[3]);
		if (isset($options[4])) $option_data[4] = $this->renderOptionDataInternalMod($options[4],($renderOptionInternalMod)?$renderOptionInternalMod['product_option_value'][$options[4]]['name']:'', $pricing[4]);
		if (isset($options[5])) $option_data[5] = $this->renderOptionDataScale($options[5],($renderOptionScale)?$renderOptionScale['product_option_value'][$options[5]]['name']:'', $pricing[5]);
		if (isset($options[6])) $option_data[6] = $this->renderOptionDataPurification($options[6],($renderOptionPurification)?$renderOptionPurification['product_option_value'][$options[6]]['name']:'', $pricing[6]);
		return $option_data;
	}

	public function calculateOligoPrice($options) {
		$settings = $this->loadSettings();
		$pricing = $this->config->get('oligo_pricing');
		$oligoPrice = 0;
		if (isset($options[1]) && $options[1] != '') $oligoPrice += $pricing[1][$options[1]][$options[5]];
		if (isset($options[3]) && $options[3] != '') $oligoPrice += $pricing[3][$options[3]][$options[5]];
		if (isset($options[4]) && $options[4] != '') $oligoPrice += $pricing[4][$options[4]][$options[5]];
		if (isset($options[6]) && $options[6] != '') $oligoPrice += $pricing[6][$options[6]][$options[5]];
		$oligoPrice += strlen($options[2]) * $pricing[5][$options[5]];
		return $oligoPrice;
	}

	private function renderOptionText($showPrices, $pricing, $id, $name, $required) {
		return array(
			'product_option_id' => $id,
			'product_option_value' => array(),
			'option_id' => $id,
			'name' => $name,
			'type' => 'text',
			'value' => '',
			'required' => $required,
		);
	}

	private function renderOptionSelect($showPrices, $pricing, $id, $name, $required, $options) {
		$select = array(
			'product_option_id' => $id,
			'product_option_value' => array(),
			'option_id' => $id,
			'name' => $name,
			'type' => 'select',
			'value' => '',
			'required' => $required
		);
		$valueId = 0;
		foreach ($options as $key => $opt) {
			$select['product_option_value'][] = array(
				'product_option_value_id' => $key,
				'option_value_id' => $key,
				'name' => $opt,
				'image' => '',
				'price' => ($showPrices)?$pricing[$key]:false,
				'price_prefix' => '',
				'quantity' => 1
			);
		}
		return $select;
	}

	private function getOptionValues($optionId, $languageId) {
		$names = $this->config->get('oligo_names');
		$index = 0;
		switch ($optionId) {
			case 1: $index = 0; break;
			case 3: $index = 1; break;
			case 4: $index = 2; break;
			case 6: $index = 3; break;
		}
		$values = array();
		foreach ($names[$index] as $val) {
			$values[] = $val[$languageId];
		}
		return $values;
	}

	private function renderOptionName($showPrices, $pricing) {
		return $this->renderOptionText($showPrices, $pricing, 0, 'Oligo Name', 1);
	}

	private function renderOption5Mod($showPrices, $pricing) {
		return $this->renderOptionSelect($showPrices, $pricing, 1, "5' Modification", 0, $this->getOptionValues(1,1));
	}

	private function renderOptionSequence($showPrices, $pricing) {
		return $this->renderOptionText($showPrices, $pricing, 2, 'Oligo Sequence', 1);
	}

	private function renderOption3Mod($showPrices, $pricing) {
		return $this->renderOptionSelect($showPrices, $pricing, 3, "3' Modification", 0, $this->getOptionValues(3,1));
	}

	private function renderOptionInternalMod($showPrices, $pricing) {
		return $this->renderOptionSelect($showPrices, $pricing, 4, 'Add Internal Modification', 0, $this->getOptionValues(4,1));
	}

	private function renderOptionScale($showPrices, $pricing) {
		return $this->renderOptionSelect($showPrices, $pricing, 5, 'Oligo Scale', 0, array('25 nmol', '50 nmol', '200 nmol', '1 umol'));
	}

	private function renderOptionPurification($showPrices, $pricing) {
		return $this->renderOptionSelect($showPrices, $pricing, 6, 'Purification', 0, $this->getOptionValues(6,1));
	}

	private function renderOptionDataField($type, $valueId, $value, $pricing, $id, $name) {
		return array(
			'product_option_id'       => $id,
			'product_option_value_id' => $valueId,
			'option_id'               => $id,
			'option_value_id'         => '',
			'name'                    => $name,
			'value'                   => $value,
			'type'                    => $type,
			'quantity'                => '',
			'subtract'                => '',
			'price'                   => $pricing,
			'price_prefix'            => '',
			'points'                  => '',
			'points_prefix'           => '',
			'weight'                  => '',
			'weight_prefix'           => ''
		);
	}

	private function renderOptionDataName($valueId, $value, $pricing) {
		return $this->renderOptionDataField('text', '', $value, $pricing, 0, $this->language->get('oligo_form_name_label'));
	}

	private function renderOptionData5Mod($valueId, $value, $pricing) {
		return $this->renderOptionDataField('select', $valueId, $value, $pricing, 1, $this->language->get('oligo_form_5_mod_label'));
	}

	private function renderOptionDataSequence($valueId, $value, $pricing) {
		return $this->renderOptionDataField('text', '', $value, $pricing, 2, $this->language->get('oligo_form_sequence_label'));
	}

	private function renderOptionData3Mod($valueId, $value, $pricing) {
		return $this->renderOptionDataField('select', $valueId, $value, $pricing, 3, $this->language->get('oligo_form_3_mod_label'));
	}

	private function renderOptionDataInternalMod($valueId, $value, $pricing) {
		return $this->renderOptionDataField('select', $valueId, $value, $pricing, 4, $this->language->get('oligo_form_internal_mod_label'));
	}

	private function renderOptionDataScale($valueId, $value, $pricing) {
		return $this->renderOptionDataField('select', $valueId, $value, $pricing, 5, $this->language->get('oligo_form_scale_label'));
	}

	private function renderOptionDataPurification($valueId, $value, $pricing) {
		return $this->renderOptionDataField('select', $valueId, $value, $pricing, 6, $this->language->get('oligo_form_purification_label'));
	}

}