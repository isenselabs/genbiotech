<?php

class ControllerModuleOligo extends Controller {
	private $imodule = 'oligo';
	private $imodule_route;
	private $imodule_model;
	private $imodule_extension_route;
	private $imodule_extension_type;
	private $imodule_debug = true;
	private $imodule_debug_uri_append = '&XDEBUG_SESSION_START=sublime.xdebug';
	private $imodule_settings = array(
		'oligo_status' => 0,
		'oligo_product_id' => 0,
		'oligo_pricing' => array(
			array(),
			array(),
			array(),
			array(),
			array(),
			array(0.45,0.55,0.75,2.5),
			array()
		),
		'oligo_names' => array(
			array(
				array('1'=>'TAMRA'),
				array('1'=>'BHQ')
			),
			array(
				array('1'=>'TAMRA'),
				array('1'=>'BHQ')
			),
			array(
				array('1'=>'TAMRA'),
				array('1'=>'BHQ')
			),
			array(
				array('1'=>'TAMRA'),
				array('1'=>'BHQ')
			)
		),
	);

	public function __construct($registry) {
		parent::__construct($registry);
		$this->load->config($this->imodule);
		$this->imodule_route = $this->config->get($this->imodule . '_route');
		$this->imodule_extension_route = $this->config->get($this->imodule . '_extension_route');
		$this->imodule_extension_type = $this->config->get($this->imodule . '_extension_type');
		$this->load->model($this->imodule_route);
		$this->imodule_model = $this->{$this->config->get($this->imodule . '_model_property')};
		$this->load->model('setting/setting');
		$this->imodule_settings['oligo_export_email'] = $this->config->get('config_email');
		$this->imodule_settings = array_merge($this->imodule_settings,$this->model_setting_setting->getSetting($this->imodule));
		$this->oligoService = new \vendor\oligo\Service($registry);
		$this->excelService = new \vendor\oligo\Phpexcel($registry);
		$this->load->model('sale/order');
	}

	public function install() {
		$this->load->model($this->imodule_route);
		$this->imodule_model->createTables();
	}

	public function uninstall() {
		$this->load->model($this->imodule_route);
		$this->imodule_model->dropTables();
	}

	private function validatePriceValue($val, &$valid) {
		if ($val == '' || $val == '0') {
			$valid = 0;
			return true;
		}
		if (is_numeric($val)) {
			$valid = floatval($val);
			return true;
		}
		return false;
	}

	private function validateIndexPost(&$errors) {
		$passes = true;
		// validate prices
		$post = $this->request->post['oligo_option_price'];
		// validate prices
		for ($i=0;$i<4;++$i) { // options 1,3,4,6
			foreach ($post[$i] as $k => $scaledPrices) {
				for ($j=0;$j<4;++$j) { // scaled price index
					$valid = 0;
					if ($this->validatePriceValue($scaledPrices[$j],$valid)) {
						$this->request->post[$i][$k][$j] = $valid;
					} else {
						// validation error
						$passes = false;
						if (!isset($errors['prices'][$i])) $errors['prices'][$i] = array();
						if (!isset($errors['prices'][$i][$j])) $errors['prices'][$i][$j] = array();
						$errors['prices'][$i][$k][] = $j;
					}
				}
			}
		}
		// the scale option
		for($k=0;$k<4;++$k) {
			$valid = 0;
			if ($this->validatePriceValue($post[4][$k], $valid)) {
				$this->request->post[4][$k] = $valid;
			} else {
				$passes = false;
				if (!isset($errors['prices'][4])) $errors['prices'][4] = array();
				$errors['prices'][4][] = $k;
			}
		}
		return $passes;
	}

	private function modSettings($settingsUpdates, $noCache=false) {
		if ($noCache) {
			$currentSettings = $this->model_setting_setting->getSetting($this->imodule);
		} else {
			$currentSettings = $this->imodule_settings;
		}
		$this->imodule_settings = array_merge($currentSettings, $settingsUpdates);
		$this->model_setting_setting->editSetting($this->imodule, $this->imodule_settings);
	}

	private function pullAlerts() {
		$alerts = isset($this->session->data['alerts'])?$this->session->data['alerts']:array();
		unset($this->session->data['alerts']);
		return $alerts;
	}

	public function index() {
		if ($this->request->server['HTTPS']) {
			$server = HTTPS_SERVER;
		} else {
			$server = HTTP_SERVER;
		}

		$this->load->model('catalog/product');

		// View data
		$language = $this->load->language($this->imodule_route);
		$data = array(
			'oligo_alerts' => $this->pullAlerts(),
			'form_action' => $this->url->link($this->imodule_route, 'token=' . $this->session->data['token'], true) . (($this->imodule_debug)?$this->imodule_debug_uri_append:''),
			'products' => $this->model_catalog_product->getProducts(array()),
			'token' => $this->session->data['token'],
			'store_mail' => $this->config->get('config_email')
		);

		// Insert the settings and language file entries
		$data = array_merge($data, $language, $this->imodule_settings);

		// Opened tab
		if (isset($this->request->get['tab'])) {
			$data['tab'] = $this->request->get['tab'];
		} else {
			$data['tab'] = 'settings';
		}

		$errors = array();
		// Saving settings
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$postPrices = $this->request->post['oligo_option_price'];
			$postNames = $this->request->post['oligo_option_name'];
			if ($this->validateIndexPost($errors)) {
				if (isset($this->request->post['oligo_product_name'])) unset($this->request->post['oligo_product_name']);
				if (isset($this->request->post['oligo_option_price'])) {
					$this->request->post['oligo_pricing'] = array(
						array(),
						$postPrices[0],
						array(),
						$postPrices[1],
						$postPrices[2],
						$postPrices[4],
						$postPrices[3]
					);
					unset($this->request->post['oligo_option_price']);
				}
				if (isset($this->request->post['oligo_option_name'])) {
					$this->request->post['oligo_names'] = array(
						$postNames[0],
						$postNames[1],
						$postNames[2],
						$postNames[3]
					);
					unset($this->request->post['oligo_option_name']);
				}
				$this->modSettings($this->request->post);
				$this->session->data['success'] = $this->language->get('text_success');
				$this->response->redirect($this->url->link($this->imodule_extension_route, 'token=' . $this->session->data['token'] . $this->imodule_extension_type, true));
			} else {
				// validation error, forward entered values to the view over saved ones
				$data = array_merge($data, $this->request->post);
				$data['oligo_pricing'] = array(
					array(),
					$postPrices[0],
					array(),
					$postPrices[1],
					$postPrices[2],
					$postPrices[4],
					$postPrices[3]
				);
				$data['oligo_names'] = array(
					$postNames[0],
					$postNames[1],
					$postNames[2],
					$postNames[3]
				);
			}
		}

		$data['errors'] = $errors;

		if (isset($errors['prices']) && count($errors['prices']) != 0) {
			$data['tab'] = 'pricing';
		}

		$data['validation_error'] = function ($i,$j,$k) use ($errors) {
			if (!isset($errors['prices'][$i])) return false;
			if ($i == 4) {
				return in_array($j, $errors['prices'][$i]);
			}
			if (!isset($errors['prices'][$i][$j])) return false;
			return in_array($k, $errors['prices'][$i][$j]);
		};

		$data['selects'] = array(
			array(
				'label_text' => $this->language->get('text_mod_5_label'),
				'help_text' => $this->language->get('text_5_mod_help'),
				'index' => 1,
				'values' => $this->imodule_settings['oligo_names'][0]
			),
			array(
				'label_text' => $this->language->get('text_mod_3_label'),
				'help_text' => $this->language->get('text_3_mod_help'),
				'index' => 3,
				'values' => $this->imodule_settings['oligo_names'][1]
			),
			array(
				'label_text' => $this->language->get('text_mod_internal_label'),
				'help_text' => $this->language->get('text_internal_mod_help'),
				'index' => 4,
				'values' => $this->imodule_settings['oligo_names'][2]
			),
			array(
				'label_text' => $this->language->get('text_purification_label'),
				'help_text' => $this->language->get('text_purification_help'),
				'index' => 6,
				'values' => $this->imodule_settings['oligo_names'][3]
			)
		);

		// find orders with oligos
		$oligoOrderIds = $this->model_module_oligo->getOrdersWithOligos();
		$oligoOrders = [];
		$oligoProductId = $this->config->get('oligo_product_id');
		foreach ($oligoOrderIds->rows as $key => $order) {
			$i = count($oligoOrders);
			$oligoOrders[] = $this->model_sale_order->getOrder($order['order_id']);
			$oligoOrders[$i]['order_status'] = $this->db->query('SELECT name FROM ' . DB_PREFIX . 'order_status WHERE order_status_id = ' . $oligoOrders[$i]['order_status_id'])->row['name'];
			$oligoOrders[$i]['order_url'] = $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $order['order_id'], true);
			$oligoOrders[$i]['excel_url'] = $this->url->link($this->imodule_route.'/generateExcelExport', 'token=' . $this->session->data['token'] . '&order_id=' . $order['order_id'], true);
			$oligoOrders[$i]['total'] = $this->currency->format($oligoOrders[$i]['total']);

			$oligoOrders[$i]['oligo_count'] = 0;
			$products = $this->model_sale_order->getOrderProducts($order['order_id']);
			foreach ($products as $key => $product) {
				if ($product['product_id'] == $oligoProductId) {
					$oligoOrders[$i]['oligo_count']++;
				}
			}
		}

		if ($this->config->get('oligo_product_id')) {
			$customOligoProduct = $this->model_catalog_product->getProduct($data['oligo_product_id']);
			if ($customOligoProduct) {
				$data['product_name'] = $customOligoProduct['name'];
			}
		}

		$data['orders'] = $oligoOrders;

		// OpenCart elements
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['breadcrumbs'] = array(
			array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
			),
			array(
				'text' => $this->language->get('text_extension'),
				'href' => $this->url->link($this->imodule_extension_route, 'token=' . $this->session->data['token'] . $this->imodule_extension_type, true)
			),
			array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link($this->imodule_route, 'token=' . $this->session->data['token'], true)
			)
		);

		// Document head
		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->addStyle($server . 'view/stylesheet/oligo.css');
		$this->document->addScript($server . 'view/javascript/oligo.js');

		$this->load->model('localisation/language');
		$data['languages'] = array();

		foreach ($this->model_localisation_language->getLanguages() as $language) {
			$data['languages'][] = array(
				'language_id' => $language['language_id'],
				'name' => $language['name'] . ($language['code'] == $this->config->get('config_language') ? $this->language->get('text_default') : ''),
				'image' => version_compare(VERSION, '2.2', '>=') ? 'language/' . $language['code'] . '/'. $language['code'] . '.png' : 'view/image/flags/' . $language['image']
			);
		}

		// Render the template
		if (version_compare(VERSION, '2.2.0.0', '>=')) {
			$tpl_path = $this->imodule_route;
		} else {
			$tpl_path = $this->imodule_route . '.tpl';
		}

		$this->response->setOutput($this->load->view($tpl_path, $data));
	}

	public function generateExcelExport() {
		$orderId = $this->request->get['order_id'];
		$order = $this->model_sale_order->getOrder($orderId);
		$products = $this->model_sale_order->getOrderProducts($orderId);
		$excel = $this->excelService->generate($order, $products, $this->model_sale_order);
		header('Content-Disposition: attachment; filename="'.$order['firstname'].'_'.$order['lastname'].'_order'.$order['order_id'].'.xls"');
		$this->excelService->save($excel, 'php://output');
		exit;
	}
}