<?php

// Heading
$_['heading_title']                                    = 'Oligo';
$_['text_edit_heading']                                = 'Settings';
$_['tab_setting']                                      = 'Extension';
$_['tab_options']                                      = 'Oligo Options';
$_['tab_pricing']                                      = 'Pricing';
$_['tab_orders']                                       = 'Orders';

$_['text_success']                                     = 'Successfully saved CustomOligo settings!';

$_['text_extension_status']                            = 'Extension status';
$_['text_extension_status_enabled']                    = 'Enabled';
$_['text_extension_status_disabled']                   = 'Disabled';
$_['text_extension_status_help']                       = 'Toggle extension status';
$_['text_product_id']                                  = 'Custom Oligo product';
$_['text_product_id_help']                             = 'Select which product to apply the options to';
$_['text_export_email']                                = 'Excel export email';
$_['text_export_email_help']                           = 'The email which will receive an Excel order whenever a customer purchases a custom oligo';

$_['text_mod_5_label']                                 = "5' Modification";
$_['text_mod_3_label']                                 = "3' Modification";
$_['text_mod_internal_label']                          = 'Internal Modification';
$_['text_purification_label']                          = 'Purification';
$_['text_scale_label']                                 = 'Scale';
$_['text_5_mod_help']                                  = '5 help';
$_['text_3_mod_help']                                  = '3 help';
$_['text_internal_mod_help']                           = 'Internal help';
$_['text_purification_help']                           = 'Purification help';
$_['text_scale_help']                                  = 'Scale help';

$_['text_values_header']                               = 'Value name';
$_['text_prices_validation_error']                     = 'Please make sure all marked fields are valid amounts';