<style>
	.custom__table_styles {
		width: 100%;
	}
	.custom__table_styles td {
		padding: 5px;
	}
	.custom__validation_error {
		border: 2px solid red;
		border-radius: 2px !important;
	}
</style>
<div id="value_price_template" style="display: none">
<section data-index="%J%">
	<pre>%NAME%</pre>
	<?php for($k=0;$k<4;$k++) { ?>
	<pre>
		<div class="input-group">
			<input type="text" step="0.01" name="oligo_option_price[%I%][%J%][<?php echo $k; ?>]" value="0" class="form-control"/>
		</div>
	</pre>
	<?php } ?>
</section>
</div>
<fieldset>
	<?php if (isset($errors['prices'])) { ?>
	<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i>&nbsp;<?php echo $text_prices_validation_error; ?>
		<button type="button" class="close" data-dismiss="alert">&times;</button>
	</div>
	<?php } ?>
	<div class="form-group oligo-score-pricing">
		<label class="col-sm-2 control-label">
			<span data-toggle="tooltip" title="<?php echo $text_scale_help; ?>"><?php echo $text_scale_label; ?></span>
		</label>
		<div class="col-sm-10">
			<table class="custom__table_styles">
				<thead><tr><th>25 nmol</th><th>50 nmol</th><th>200 nmol</th><th>1 umol</th></tr></thead>
				<tbody>
					<tr>
						<td>
							<div class="input-group">
								<input type="text" step="0.01" name="oligo_option_price[4][0]" value="<?php echo $oligo_pricing[5][0]; ?>" class="form-control<?php echo ($validation_error(4,0,-1))?" custom__validation_error":""; ?>"/>
							</div>
						</td>
						<td>
							<div class="input-group">
								<input type="text" step="0.01" name="oligo_option_price[4][1]" value="<?php echo $oligo_pricing[5][1]; ?>" class="form-control<?php echo ($validation_error(4,1,-1))?" custom__validation_error":""; ?>"/>
							</div>
						</td>
						<td>
							<div class="input-group">
								<input type="text" step="0.01" name="oligo_option_price[4][2]" value="<?php echo $oligo_pricing[5][2]; ?>" class="form-control<?php echo ($validation_error(4,2,-1))?" custom__validation_error":""; ?>"/>
							</div>
						</td>
						<td>
							<div class="input-group">
								<input type="text" step="0.01" name="oligo_option_price[4][3]" value="<?php echo $oligo_pricing[5][3]; ?>" class="form-control<?php echo ($validation_error(4,3,-1))?" custom__validation_error":""; ?>"/>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<?php $i = 0; foreach ($selects as $select) : ?>
	<div class="form-group" data-index="<?php echo $i; ?>">
		<label class="col-sm-2 control-label">
			<span data-toggle="tooltip" title="<?php echo $select['help_text']; ?>"><?php echo $select['label_text']; ?></span>
		</label>
		<div class="col-sm-10">
			<table class="custom__table_styles">
				<thead><tr><th><?php echo $text_values_header; ?></th><th>25 nmol</th><th>50 nmol</th><th>200 nmol</th><th>1 umol</th></tr></thead>
				<tbody>
				<?php $j = 0; foreach ($select['values'] as $val) : ?>
					<tr data-index="<?php echo $j; ?>">
						<td><?php echo $val['1']; ?></td>
						<?php for($k=0;$k<4;$k++) { ?>
						<td>
							<div class="input-group">
								<input type="text" step="0.01" name="oligo_option_price[<?php echo $i; ?>][<?php echo $j; ?>][<?php $k; ?>]" value="<?php echo (isset($oligo_pricing[$select['index']][$j]) && isset($oligo_pricing[$select['index']][$j][$k]))?$oligo_pricing[$select['index']][$j][$k]:''; ?>" class="form-control<?php echo ($validation_error($i,$j,$k))?" custom__validation_error":""; ?>"/>
							</div>
						</td>
						<?php } ?>
					</tr>
				<?php $j++; endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
	<?php $i++; endforeach; ?>
</fieldset>