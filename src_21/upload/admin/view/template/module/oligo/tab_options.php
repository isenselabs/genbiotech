<style>
	.custom__input__group__styles {
	    margin-bottom: 5px;
    	margin-right: 10px;
	}
	.options__table tr {
	    border-bottom: 15px solid transparent;
	}
	.options__table .options__table__values_header {
		border-bottom: 5px solid transparent;
	}
</style>
<div id="value_name_template" style="display: none;">
<section data-index="%VALUEID%">
	<pre>
		<?php foreach($languages as $language) : ?>
		<div class="input-group custom__input__group__styles">
			<span class="input-group-addon"><img src="<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" /></span>
			<input type="text" name="oligo_option_name[%OPTIONID%][%VALUEID%][<?php echo $language['language_id']; ?>]" value="" class="form-control"/>
		</div>
		<?php endforeach; ?>
	</pre>
	<pre>
		<a href="#" class="removeValueBtn btn btn-danger"><i class="fa fa-trash-o"></i></a>
	</pre>
</section>
</div>
<fieldset>
	<?php $i = 0; foreach ($selects as $select) : ?>
		<div class="form-group" data-index="<?php echo $i; ?>">
			<label class="col-sm-2 control-label">
				<span data-toggle="tooltip" title="<?php echo $select['help_text']; ?>"><?php echo $select['label_text']; ?></span>
			</label>
			<div class="col-sm-10">
				<table class="options__table">
					<thead><tr class="options__table__values_header"><th colspan="2"><?php echo $text_values_header; ?></th></tr></thead>
					<tbody>
						<?php $j = 0; foreach ($select['values'] as $val) : ?>
						<tr data-index="<?php echo $j ?>">
							<td>
								<?php foreach($languages as $language) : ?>
								<div class="input-group custom__input__group__styles">
									<span class="input-group-addon"><img src="<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" /></span>
									<input type="text" name="oligo_option_name[<?php echo $i; ?>][<?php echo $j; ?>][<?php echo $language['language_id']; ?>]" value="<?php echo !empty($val[$language['language_id']])?$val[$language['language_id']]:'' ?>" class="form-control"/>
								</div>
								<?php endforeach; ?>
							</td>
							<td>
								<a href="#" class="removeValueBtn btn btn-danger"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
						<?php $j++; endforeach; ?>
						<tr>
							<td>
								<a href="#" class="addValueBtn btn btn-primary"><i class="fa fa-plus"></i></a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	<?php $i++; endforeach; ?>
</fieldset>