<div class="table-responsive">
	<table class="table table-bordered table-hover">
		<thead>
			<tr><th>Order ID</th><th>Placed</th><th>Customer</th><th>Oligos</th><th>Order total</th><th>Order status</th><th>Excel</th></tr>
		</thead>
		<tbody>
			<?php foreach ($orders as $order): ?>
			<tr>
				<td><a href="<?php echo $order['order_url']; ?>"><?php echo $order['order_id']; ?></a></td>
				<td><?php echo $order['date_modified']; ?></td>
				<td><?php echo $order['customer']; ?></td>
				<td><?php echo $order['oligo_count']; ?></td>
				<td><?php echo $order['total']; ?></td>
				<td><?php echo print_r($order['order_status'],true); ?></td>
				<td><a href="<?php echo $order['excel_url']; ?>">Export to Excel</a></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>