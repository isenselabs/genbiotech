<?php
echo $header;
echo $column_left;
?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="oligo-admin-form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php foreach ($oligo_alerts as $alert) { ?>
			<div class="alert alert-<?php echo $alert['type']; ?>"><i class="fa fa-<?php echo $alert['icon']; ?>"></i>&nbsp;<?php echo $alert['text']; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i>&nbsp;<?php echo $text_edit_heading; ?></h3>
			</div>

			<div class="panel-body">
				<form action="<?php echo $form_action; ?>" method="post" enctype="multipart/form-data" id="oligo-admin-form" class="form-horizontal">
					<ul class="nav nav-tabs">
                        <li><a class=z href="#tab-settings" data-toggle="tab"><i class="fa fa-gear"></i>&nbsp;<?php echo $tab_setting; ?></a></li>
                        <li><a href="#tab-options" data-toggle="tab"><i class="fa fa-list"></i>&nbsp;<?php echo $tab_options; ?></a></li>
                        <li><a href="#tab-pricing" data-toggle="tab"><i class="fa fa-list"></i>&nbsp;<?php echo $tab_pricing; ?></a></li>
                        <li><a href="#tab-orders" data-toggle="tab"><i class="fa fa-list"></i>&nbsp;<?php echo $tab_orders; ?></a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane" id="tab-settings">
							<fieldset>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="dropdown_oligo_status"><span data-toggle="tooltip" title="<?php echo $text_extension_status_help; ?>"><?php echo $text_extension_status; ?></span></label>
									<div class="col-sm-10">
										<select name="oligo_status" id="dropdown_oligo_status" class="form-control">
											<option value="1"<?php if ($oligo_status == 1) echo ' selected="selected"'; ?>><?php echo $text_extension_status_enabled; ?></option>
											<option value="0"<?php if ($oligo_status == 0) echo ' selected="selected"'; ?>><?php echo $text_extension_status_disabled; ?></option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="oligo_product_name"><span data-toggle="tooltip" title="<?php echo $text_product_id_help; ?>"><?php echo $text_product_id; ?></span></label>
									<div class="col-sm-10">
										<input type="text" name="oligo_product_name" id="oligo_product_name" class="form-control col-sm-10" value="<?php echo $product_name; ?>"/>
										<input type="hidden" name="oligo_product_id" id="oligo_product_id" class="form-control col-sm-10" value="<?php echo $oligo_product_id; ?>"/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="oligo_export_email"><span data-toggle="tooltip" title="<?php echo $text_export_email_help; ?>"><?php echo $text_export_email; ?></span></label>
									<div class="col-sm-10">
										<input type="text" name="oligo_export_email" id="oligo_export_email" class="form-control col-sm-10" value="<?php echo ($oligo_export_email)?$oligo_export_email : $store_mail; ?>"/>
									</div>
								</div>
							</fieldset>
						</div>
						<div class="tab-pane" id="tab-options">
							<?php require_once DIR_APPLICATION.'view/template/module/oligo/tab_options.php'; ?>
						</div>
						<div class="tab-pane" id="tab-pricing">
							<?php require_once DIR_APPLICATION.'view/template/module/oligo/tab_pricing.php'; ?>
						</div>
						<div class="tab-pane" id="tab-orders">
							<?php require_once DIR_APPLICATION.'view/template/module/oligo/tab_orders.php'; ?>
						</div>
					</div>
				</form>
			</div>
	</div>
</div>
<script type="text/javascript" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
function removeValue(optionId, valueId) {
	$('#tab-options fieldset .form-group[data-index="'+optionId+'"]').not('.oligo-score-pricing').find('tbody tr[data-index="'+valueId+'"]').remove();
	$('#tab-pricing fieldset .form-group[data-index="'+optionId+'"]').not('.oligo-score-pricing').find('tbody tr[data-index="'+valueId+'"]').remove();
	// reindex the values after the removed one
	$('#tab-options fieldset .form-group[data-index="'+optionId+'"]').not('.oligo-score-pricing').find('tbody tr').each(function(i) {
		if (i >= valueId) {
			$(this).attr('data-index',i);
			$(this).find('[name^="oligo_option_name['+optionId+']"]').each(function(j) {
				let name = $(this).attr('name').split('oligo_option_name['+optionId+']"][' + (i+1) + ']').join('oligo['+i+']');
				$(this).attr('name', name);
			});
		}
	});
	$('#tab-pricing fieldset .form-group[data-index="'+optionId+'"]').not('.oligo-score-pricing').find('tbody tr').each(function(i) {
		if (i >= valueId) {
			$(this).attr('data-index',i);
			$(this).find('[name^="oligo_option_price['+optionId+']"]').each(function(j) {
				let name = $(this).attr('name').split('oligo_option_price['+optionId+']"][' + (i+1) + ']').join('oligo['+i+']');
				$(this).attr('name', name);
			});
		}
	});
}

function addValue(optionId) {
	let nameTemplate = $('#value_name_template').html();
	nameTemplate = nameTemplate.split('%OPTIONID%').join(optionId);
	nameTemplate = nameTemplate.split('%VALUEID%').join(optionCounters[optionId]);
	nameTemplate = nameTemplate.split('section').join('tr');
	nameTemplate = nameTemplate.split('pre').join('td');
	nameTemplate += '<tr><td colspan="2"><a href="#" class="addValueBtn btn btn-primary"><i class="fa fa-plus"></i></a></td></tr>';
	
	let priceTemplate = $('#value_price_template').html();
	priceTemplate = priceTemplate.split('%I%').join(optionId);
	let j = $('#tab-options .form-group[data-index="'+optionId+'"] tr[data-index]').length;
	priceTemplate = priceTemplate.split('%J%').join(j);
	priceTemplate = priceTemplate.split('%NAME%').join('');
	priceTemplate = priceTemplate.split('section').join('tr');
	priceTemplate = priceTemplate.split('pre').join('td');

	$('#tab-options fieldset .form-group[data-index="'+optionId+'"] tbody').append(nameTemplate);
	$('#tab-pricing fieldset .form-group[data-index="'+optionId+'"] tbody').append(priceTemplate);
}

let optionCounters = [<?php echo count($selects[0]['values']); ?>,<?php echo count($selects[1]['values']); ?>,<?php echo count($selects[2]['values']); ?>,<?php echo count($selects[3]['values']); ?>];

function onRemoveValue(e) {
	e.preventDefault();
	let optionId = $(this).closest('.form-group').data('index');
	let valueId = $(this).closest('tr').data('index');
	removeValue(optionId, valueId);
	optionCounters[optionId]--;
}

function onAddValue(e) {
	e.preventDefault();
	let optionId = $(this).closest('.form-group').data('index');
	$(this).closest('tr').remove();
	addValue(optionId);
	optionCounters[optionId]++;
}

function onUpdateName(e) {
	let optionId = $(this).closest('.form-group').data('index');
	let valueId = $(this).closest('tr').data('index');
	$('#tab-pricing fieldset .form-group[data-index="'+optionId+'"] tbody tr[data-index="'+valueId+'"] td').first().html($(this).val());
}

$(document).ready(function() {
	$('.nav-tabs a[href="#tab-<?php echo $tab ?>"]').tab('show');
	$('input[name=\'oligo_product_name\']').autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['product_id']
						}
					}));
				}
			});
		},
		'select': function(item) {
			$('input[name=\'oligo_product_name\']').val(item['label']);
			$('input[name=\'oligo_product_id\']').val(item['value']);
		}
	});

	$('#tab-options fieldset table').on('click', '.removeValueBtn', onRemoveValue);
	$('#tab-options fieldset table').on('click', '.addValueBtn', onAddValue);
	$('#tab-options fieldset table').on('change', 'tbody td .input-group:nth-child(1) input', onUpdateName);

	$('#tab-orders .table').DataTable({
        'initComplete': function(settings, json) {
        	$('.dataTables_length select').css({
			    'height' : '35px',
			    'padding' : '8px 13px',
			    'font-size' : '12px',
			    'line-height' : '1.42857',
			    'color' : '#555',
			    'background-color' : '#fff',
			    'background-image' : 'none',
			    'border' : '1px solid #ccc',
			    'border-radius' : '3px',
			    '-webkit-box-shadow' : 'inset 0 1px 1px rgba(0, 0, 0, .075)',
			    'box-shadow' : 'inset 0 1px 1px rgba(0, 0, 0, .075)',
			    '-webkit-transition' : 'border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s',
			    '-o-transition' : 'border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s',
			    'transition' : 'border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s'
        	});

            $('.dataTables_filter').css('margin', '10px 0 15px');
            $('.dataTables_filter input').addClass('form-control').css('margin-top', '5px');

            $('.dataTables_paginate').css('margin', '15px 0');
            $('.dataTables_paginate .paginate_button.previous').addClass('btn').addClass('btn-default');
            $('.dataTables_paginate .paginate_button.next').addClass('btn').addClass('btn-default');
            $('.dataTables_paginate span').css('padding', '0 15px');
        }
    });
});
</script>
<?php echo $footer; ?>