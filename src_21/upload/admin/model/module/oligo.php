<?php

class ModelModuleOligo extends Model {
	private $imodule = 'oligo';
	private $imodule_route;
	private $imodule_model;
	private $imodule_extension_route;
	private $imodule_extension_type;

	public function __construct($registry) {
		parent::__construct($registry);
		$this->load->config($this->imodule);
		$this->imodule_route = $this->config->get($this->imodule . '_route');
		$this->imodule_extension_route = $this->config->get($this->imodule . '_extension_route');
		$this->imodule_extension_type = $this->config->get($this->imodule . '_extension_type');
		$this->imodule_model = $this->{$this->config->get($this->imodule . '_model_property')};
	}

	public function createTables() {

	}

	public function dropTables() {
		
	}

	public function getOrdersWithOligos() {
		$sql = 'SELECT DISTINCT orderproduct.order_id FROM ' . DB_PREFIX . 'order_product as orderproduct LEFT JOIN ' . DB_PREFIX . 'order as ocorder ON orderproduct.order_id = ocorder.order_id WHERE ocorder.order_status_id != 0 AND product_id = ' . $this->config->get('oligo_product_id') . ' ORDER BY order_id DESC';
		return $this->db->query($sql);
	}

}